= Install ```git```
:experimental: true


    apt update
    apt install git

create an ```ssh``` key for use with *gitlab.com* or *github.com* substituting whatever you want in 'your-user-name' and 'your-domain' below. I have assumed that you have a ```.com``` domain but you can work out what to do.

    ssh-keygen -t ed25519 -C "your-user-name@your-domain.com"

I hit kbd:[enter] three times to accept the default filename and use no passkey.

    cat .ssh/id_ed25519.pub

and copy the line starting ```ssh-ed25519``` into the appropriate place in gitlab or github.

